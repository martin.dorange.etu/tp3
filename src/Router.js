export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];

	static #menuElement;
	static set menuElement(element) {
		this.#menuElement = element;
		// au clic sur n'importe quel lien contenu dans "element"
		// déclenchez un appel à Router.navigate(path)
		// où "path" est la valeur de l'attribut `href=".."` du lien cliqué
		const links = element.querySelectorAll('a');
		console.log(links);
		for (let i = 0; i < links.length; i++) {
			links[i].addEventListener('click', event => {
				event.preventDefault();
				Router.navigate(links[i].getAttribute('href'));
				console.log('element : ' + links[i]);
				console.log('attribut de element : ' + links[i].getAttribute('href'));
			});
		}
	}

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
		}
	}
}
